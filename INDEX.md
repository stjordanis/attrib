# attrib

Display and set file attributes


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## ATTRIB.LSM

<table>
<tr><td>title</td><td>attrib</td></tr>
<tr><td>version</td><td>2.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2003-07-01</td></tr>
<tr><td>description</td><td>Display and set file attributes</td></tr>
<tr><td>keywords</td><td>freedos, attrib</td></tr>
<tr><td>author</td><td>pbrutsch@creighton.edu (Phil Brutsche)</td></tr>
<tr><td>maintained&nbsp;by</td><td>reifsnyderb@mindspring.com (Brian E. Reifsnyder)</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Attrib</td></tr>
</table>
